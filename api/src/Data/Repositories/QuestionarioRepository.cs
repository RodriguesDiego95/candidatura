using System;
using System.Collections.Generic;
using System.IO;
using Data.Repositories.Interfaces;
using Domain.Entities;
using Newtonsoft.Json;

namespace Data.Repositories
{
    public class QuestionarioRepository : IQuestionarioRepository
    {
        private static readonly string pathQuestionarios = Directory.GetCurrentDirectory() + @"\src\Data\Database\Questionarios.json";
        public static readonly string pathRespostas = Directory.GetCurrentDirectory() + @"\src\Data\Database\Respostas.json";

        public void Inserir(Questionario questionario)
        {
            //var JSONresult = JsonConvert.SerializeObject(questionario);
            questionario.DataCadastro = DateTime.Now;
            var questionarios = Get();
            questionarios.Add(questionario);
            SaveQuestionarios(questionarios);
        }
        
        public List<Questionario> Get()
        {
            var questionarios = new List<Questionario>();
            if (File.Exists(pathQuestionarios))
            {
                questionarios = GetDatabaseQuestionarios();
            }
            return questionarios;
        }

        public List<Questionario> Get(string usuario)
        {
            var questionarios = new List<Questionario>();
            if (File.Exists(pathQuestionarios))
            {
                var allQuestionarios = GetDatabaseQuestionarios();
                questionarios = allQuestionarios.FindAll((questionario) => questionario.Usuario == usuario);
            }
            return questionarios;
        }

        public List<Questionario> GetOthers(string usuario)
        {
            var questionarios = new List<Questionario>();
            if (File.Exists(pathQuestionarios))
            {
                var allQuestionarios = GetDatabaseQuestionarios();
                questionarios = allQuestionarios.FindAll((questionario) => questionario.Usuario != usuario);
            }
            return questionarios;
        }

        private List<Questionario> GetDatabaseQuestionarios()
        {
            var streamReader = new StreamReader(pathQuestionarios);
            var questionarios = JsonConvert.DeserializeObject<List<Questionario>>(streamReader.ReadToEnd());
            streamReader.Close();
            return questionarios;
        }

        private void SaveQuestionarios(List<Questionario> questionarios)
        {
            if (File.Exists(pathQuestionarios))
            {
                File.Delete(pathQuestionarios);
                var json = JsonConvert.SerializeObject(questionarios);
                var streamWriter = new StreamWriter(pathQuestionarios, true);
                streamWriter.WriteLine(json.ToString());
                streamWriter.Close();
            }
        }
    }
}