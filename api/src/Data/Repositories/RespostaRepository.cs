using System;
using System.Collections.Generic;
using System.IO;
using Data.Repositories.Interfaces;
using Domain.Entities;
using Newtonsoft.Json;

namespace Data.Repositories
{
    public class RespostaRepository : IRespostaRepository
    {
        private static readonly string pathQuestionarios = Directory.GetCurrentDirectory() + @"\src\Data\Database\Questionarios.json";
        public static readonly string pathRespostas = Directory.GetCurrentDirectory() + @"\src\Data\Database\Respostas.json";

        public void Inserir(Resposta resposta)
        {
            var JSONresult = JsonConvert.SerializeObject(resposta);
            var respostas = Get();
            resposta.DataCadastro = DateTime.Now;
            respostas.Add(resposta);
            SaveRespostas(respostas);
        }

        public List<Resposta> Get(string idQuestionario)
        {
            var respostas = new List<Resposta>();
            if (File.Exists(pathRespostas))
            {
                var allRespostas = GetDatabaseRespostas();
                respostas = allRespostas.FindAll((resposta) => resposta.IdQuestionario == idQuestionario);
            }
            return respostas;
        }

        private List<Resposta> Get()
        {
            var respostas = new List<Resposta>();
            if (File.Exists(pathRespostas))
            {
                respostas = GetDatabaseRespostas();
            }
            return respostas;
        }

        private List<Resposta> GetDatabaseRespostas()
        {
            var streamReader = new StreamReader(pathRespostas);
            var respostas = JsonConvert.DeserializeObject<List<Resposta>>(streamReader.ReadToEnd());
            streamReader.Close();
            return respostas;
        }

        private void SaveRespostas(List<Resposta> respostas)
        {
            if (File.Exists(pathRespostas))
            {
                File.Delete(pathRespostas);
                var json = JsonConvert.SerializeObject(respostas);
                var streamWriter = new StreamWriter(pathRespostas, true);
                streamWriter.WriteLine(json.ToString());
                streamWriter.Close();
            }
        }
    }
}