using System.Collections.Generic;
using Domain.Entities;

namespace Data.Repositories.Interfaces
{
    public interface IRespostaRepository
    {
         void Inserir(Resposta resposta);
         List<Resposta> Get(string idQuestionario);
    }
}