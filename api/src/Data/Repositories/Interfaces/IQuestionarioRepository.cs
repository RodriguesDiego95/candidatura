using System.Collections.Generic;
using Domain.Entities;

namespace Data.Repositories.Interfaces
{
    public interface IQuestionarioRepository
    {
         void Inserir(Questionario questionario);
         List<Questionario> Get();
         List<Questionario> Get(string usuario);
         List<Questionario> GetOthers(string usuario);
    }
}