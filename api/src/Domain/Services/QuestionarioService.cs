using System;
using System.Collections.Generic;
using System.IO;
using Data.Repositories;
using Data.Repositories.Interfaces;
using Domain.Entities;
using Domain.Services.Interfaces;
using Newtonsoft.Json;

namespace Domain.Services
{
    public class QuestionarioService : IQuestionarioService
    {
        private readonly IQuestionarioRepository _questionarioRepository;

        public QuestionarioService(IQuestionarioRepository questionarioRepository)
        {
            _questionarioRepository = questionarioRepository;
        }

        public void Inserir(Questionario questionario)
        {
            _questionarioRepository.Inserir(questionario);
        }

        public List<Questionario> Get()
        {
            var questionarios = _questionarioRepository.Get();
            return questionarios;
        }

        public List<Questionario> Get(string usuario)
        {
            var questionarios = _questionarioRepository.Get(usuario);
            return questionarios;
        }

        public List<Questionario> GetOthers(string usuario)
        {
            var questionarios = _questionarioRepository.GetOthers(usuario);
            return questionarios;
        }
    }
}