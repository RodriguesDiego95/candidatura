using System.Collections.Generic;
using Data.Repositories.Interfaces;
using Domain.Entities;
using Domain.Services.Interfaces;

namespace Domain.Services
{
    public class RespostaService : IRespostaService
    {
        private readonly IRespostaRepository _respostaRepository;
        public RespostaService(IRespostaRepository respostaRepository)
        {
            _respostaRepository = respostaRepository;
        }

        public bool Inserir(Resposta resposta)
        {
            if (ValidaReposta(resposta))
            {
                _respostaRepository.Inserir(resposta);
                return true;
            }
            return false;
        }

        public List<Resposta> Get(string idQuestionario)
        {
            var respostas = _respostaRepository.Get(idQuestionario);
            return respostas;
        }

        private bool ValidaReposta(Resposta resposta)
        {
            return !ExisteCamposEmBranco(resposta);
        }

        private bool ExisteCamposEmBranco(Resposta resposta)
        {
            var camposEmBranco = 
                 string.IsNullOrEmpty(resposta.Descricao) 
                || string.IsNullOrEmpty(resposta.Latitude) 
                || string.IsNullOrEmpty(resposta.Longitude) 
                || string.IsNullOrEmpty(resposta.Usuario);
            return camposEmBranco;
        }
    }
}