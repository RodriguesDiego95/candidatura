using System.Collections.Generic;
using Domain.Entities;

namespace Domain.Services.Interfaces
{
    public interface IQuestionarioService 
    {
        void Inserir(Questionario questionario);
        List<Questionario> Get();
        List<Questionario> Get(string usuario);
        List<Questionario> GetOthers(string usuario);
    }
}