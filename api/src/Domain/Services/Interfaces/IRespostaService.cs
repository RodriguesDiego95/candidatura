using System.Collections.Generic;
using Domain.Entities;

namespace Domain.Services.Interfaces
{
    public interface IRespostaService 
    {
        bool Inserir(Resposta resposta);
        List<Resposta> Get(string idQuestionario);
    }
}