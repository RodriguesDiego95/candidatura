using System;

namespace Domain.Entities
{
    public class Resposta
    {
        private string _descricao;
        private string _usuario;
        private string _latitude;
        private string _longitude;
        private string _idQuestionario;
        public DateTime DataCadastro {get;set;}
        public string IdQuestionario {
            get {
                return _idQuestionario ?? string.Empty;
            }
            set {
                _idQuestionario = value;
            }
        }
        public string Descricao
        {
            get
            {
                return _descricao ?? string.Empty;
            }
            set
            {
                _descricao = value;
            }
        }
        public string Usuario
        {
            get
            {
                return _usuario ?? string.Empty;
            }
            set
            {
                _usuario = value;
            }
        }
        public string Latitude
        {
            get
            {
                return _latitude ?? string.Empty;
            }
            set
            {
                _latitude = value;
            }
        }

        public string Longitude
        {
            get
            {
                return _longitude ?? string.Empty;
            }
            set
            {
                _longitude = value;
            }
        }
    }
}