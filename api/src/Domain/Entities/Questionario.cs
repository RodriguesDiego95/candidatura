using System;

namespace Domain.Entities
{
    public class Questionario
    {
        private string _titulo;
        private string _usuario;
        private string _id { get; set; }
        public DateTime DataCadastro { get; set; }
        public string Id
        {
            get
            {
                return _id ?? string.Empty;
            }
            set
            {
                _id = value;
            }
        }
        public string Titulo
        {
            get
            {
                return _titulo ?? string.Empty;
            }
            set
            {
                _titulo = value;
            }
        }
        public string Usuario
        {
            get
            {
                return _usuario ?? string.Empty;
            }
            set
            {
                _usuario = value;
            }
        }
    }
}