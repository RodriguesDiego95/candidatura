using Domain.Entities;
using Domain.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Presentation.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class QuestionarioController : Controller
    {
        private readonly ILogger<QuestionarioController> _logger;
        private IQuestionarioService _questionarioService;

        public QuestionarioController(ILogger<QuestionarioController> logger, IQuestionarioService questionarioService)
        {
            _logger = logger;
            _questionarioService = questionarioService;
        }
        
        [HttpPost]
        public IActionResult Post(Questionario questionario)
        {
            try
            {
                _questionarioService.Inserir(questionario);
                return Ok(new
                {
                    Message = "Inserido com sucesso!"
                });
            }
            catch
            {
                return BadRequest(new
                {
                    Message = "Houve um erro ao inserir questionário."
                });
            }
        }

        [HttpGet]
        public IActionResult Get()
        {
            try
            {
                var questionarios = _questionarioService.Get();
                return Ok(new
                {
                    Message = "Recuperados com sucesso!",
                        questionarios = questionarios
                });
            }
            catch
            {
                return BadRequest(new
                {
                    Message = "Houve um erro ao recuperar questionários."
                });
            }
        }

        [HttpGet]
        [Route("/questionarioothers")]
        public IActionResult GetQuestionarioOthers(string usuario)
        {
            try
            {
                var questionarios = _questionarioService.GetOthers(usuario);
                return Ok(new
                {
                    Message = "Recuperados com sucesso!",
                        questionarios = questionarios
                });
            }
            catch
            {
                return BadRequest(new
                {
                    Message = "Houve um erro ao recuperar questionários."
                });
            }
        }

        [HttpGet]
        [Route("/questionariouser")]
        public IActionResult GetQuestionarioUser(string usuario)
        {
            try
            {
                var questionarios = _questionarioService.Get(usuario);
                return Ok(new
                {
                    Message = "Recuperados com sucesso!",
                        questionarios = questionarios
                });
            }
            catch
            {
                return BadRequest(new
                {
                    Message = "Houve um erro ao recuperar questionários."
                });
            }
        }
    }
}