using Domain.Entities;
using Domain.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Presentation.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class RespostaController : Controller
    {
        private readonly ILogger<RespostaController> _logger;
        private IRespostaService _respostaService;

        public RespostaController(ILogger<RespostaController> logger, IRespostaService respostaService)
        {
            _logger = logger;
            _respostaService = respostaService;
        }

        [HttpPost]
        public IActionResult Post(Resposta resposta)
        {
            try
            {
                if (_respostaService.Inserir(resposta))
                {
                    return Ok(new
                    {
                        Message = "Resposta inserida com sucesso!"
                    });
                }
                else
                {
                    return NotFound(new
                    {
                        Message = "Não foi possível inserir a resposta! Por favor, preencha todas as informações."
                    });
                }
            }
            catch
            {
                return BadRequest(new
                {
                    Message = "Houve um erro ao inserir questionário."
                });
            }
        }

        [HttpGet]
        public IActionResult Get(string idQuestionario)
        {
            try
            {
                var respostas = _respostaService.Get(idQuestionario);
                return Ok(new
                {
                    Message = "Recuperados com sucesso!",
                        respostas = respostas
                });
            }
            catch
            {
                return BadRequest(new
                {
                    Message = "Houve um erro ao recuperar questionários."
                });
            }
        }
    }
}