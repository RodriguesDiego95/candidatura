using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace api
{
    public class Program
    {
        public static readonly string pathQuestionarios = Directory.GetCurrentDirectory()+@"\src\Data\Database\Questionarios.json";
        public static readonly string pathRespostas = Directory.GetCurrentDirectory()+@"\src\Data\Database\Respostas.json";
        
        public static void Main(string[] args)
        {
            var paths = new List<string>
            {
                pathQuestionarios,
                pathRespostas
            };
            CreateDatabase(paths);
            CreateHostBuilder(args).Build().Run();
        }

        private static void CreateDatabase(List<string> paths)
        {
            foreach (var path in paths)
            {
                if (!File.Exists(path))
                {
                    using(var tw = new StreamWriter(path, true))
                    {
                        var json = JsonConvert.SerializeObject(new List<object>());
                        tw.WriteLine(json.ToString());
                        tw.Close();
                    }
                }
            }
        }
        
        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
            .ConfigureWebHostDefaults(webBuilder =>
            {
                webBuilder.UseStartup<Startup>();
            });
    }
}