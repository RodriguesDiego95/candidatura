// Server
function getApi() {
    var servidor = "http://localhost:5000/";
    return servidor;
}

function renderHtml(parent, content, callback) {
    $(parent).load(content, function () {
        eval(callback);
    });
}

function formatDate(date) {
    // Converte datas do formato do JSON AAAA-MM-DD para o britânico: DD/MM/AAAA
    if (date.length < 10) {
        return ""
    }
    var ret = date.substring(8, 10) + "/";
    ret += date.substring(5, 7) + "/";
    ret += date.substring(0, 4);
    return ret
}