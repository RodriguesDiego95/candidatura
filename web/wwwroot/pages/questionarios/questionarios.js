function questionariosIndex() {
    renderHtml("#app", "/pages/questionarios/questionarios.html", "loadQuestionarios();");
}

function loadQuestionarios() {
    $.ajax({
        type: "GET",
        url: getApi() + "questionariouser/?usuario="+$("#user").val(),
        cache: false,
        dataType: "json",
        contentType: "application/json;charset=utf-8",
        success: function (retorno) {
            preencheListaMinhasPerguntas(retorno.questionarios);
        },
        error: function () {
            alert("Ocorreu um erro ao recuperar questionários");
        }
    });
}

function preencheListaMinhasPerguntas(questionarios) {
    $("#minhas-perguntas").html(`
        <div class='card'>
            <div class='card-header'>
                <h4>Minhas Perguntas<h4>
            </div>
            <div class='card-body'>
            </div>
        </div>
    `)

    if (questionarios.length > 0) {
        questionarios.map(questionario => {
            $("#minhas-perguntas > .card > .card-body").append(`
                <div id=${questionario.id} onclick="VerRespostas(${questionario.id})" class="cursor-pointer"
                  data-bs-toggle="collapse" data-bs-target="#ver-respostas${questionario.id}"
                  aria-expanded="false" aria-controls="ver-respostas${questionario.id}"> >
                    <span>${questionario.titulo}</span>
                </div>
                <div id="ver-respostas${questionario.id}" class="collapse">
                </div>
                <hr />
            `)
        });
    } 
    else {
        $("#minhas-perguntas > .card > .card-body").append(`
            <strong>Você ainda não registrou perguntas :(</strong>
        `)
    }
}

function inserirPergunta() {
    if ($("#inputNovaPergunta").val() !== "") {
        $.ajax({
            type: "POST",
            url: getApi() + "Questionario",
            data: JSON.stringify(retornaDadosQuestionario()),
            cache: false,
            contentType: "application/json;charset=utf-8",
            success: function (retorno) {
                $("#inputNovaPergunta").val("")
                loadQuestionarios();
            },
            error: function () {
                alert("Ocorreu um erro ao salvar o questionário.");
            }
        });
    }
}

function retornaDadosQuestionario() {
    var dados = {
        Titulo: $("#inputNovaPergunta").val(),
        Usuario: $("#user").val(),
        Id: Date.now().toString()
    };
    return dados;
}

function VerRespostas(idQuestionario) {
    $.ajax({
        type: "GET",
        url: getApi() + "Resposta?idquestionario=" + idQuestionario,
        cache: false,
        contentType: "application/json;charset=utf-8",
        success: function (retorno) {
            loadRespostas(idQuestionario, retorno.respostas);
        },
        error: function () {
            alert("Ocorreu um erro ao recuperar respostas");
        }
    });
}

function loadRespostas(idQuestionario, respostas) {
    if (respostas.length > 0) {
        $('#ver-respostas'+idQuestionario).html('')
        respostas.map(resposta => {
            $('#ver-respostas'+resposta.idQuestionario).append(`
                <div class='respostas'>
                    <p>
                        <strong>
                            Resposta: ${resposta.descricao}
                        </strong>  
                        <br/>
                        Usuário: ${resposta.usuario} <br/>
                        Data: ${formatDate(resposta.dataCadastro)} <br/>
                        Latitude: ${resposta.latitude} <br/>
                        Longitude: ${resposta.longitude} <br/>
                    </p>
                </div>
            `)
        })
    } else {
        $('#ver-respostas'+idQuestionario).html(`
            <strong>Você ainda não possui respostas para essa pergunta! :(</strong>
        `)
    }
}