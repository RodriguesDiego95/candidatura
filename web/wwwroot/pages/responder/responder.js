function responderIndex() {
    renderHtml("#app", "/pages/responder/responder.html", "loadResponder();");
}

function loadResponder() {
    $.ajax({
        type: "GET",
        url: getApi() + "questionarioothers/?usuario="+$("#user").val(),
        cache: false,
        dataType: "json",
        contentType: "application/json;charset=utf-8",
        success: function (retorno) {
            preencheListaResponder(retorno.questionarios);
        },
        error: function () {
            alert("Ocorreu um erro ao recuperar questionários");
        }
    });
}

function preencheListaResponder(questionarios) {
    if (questionarios.length > 0) {
        $("#painel-responder").html(`
            <div class='card'>
                <div class='card-header'> 
                    <h4>Você possui ${questionarios.length} pergunta(s)  para responder!<h4>
                </div>
                <div class='card-body'>
                </div>
            </div>
        `)
    } else {
        $("#painel-responder").html(`
            <h4>
                Por enquanto não temos pergunta para você responder! ):
            </h4>
        `)
    }

    questionarios.map(questionario => {
        $("#painel-responder > .card > .card-body").append(`
            <div id=${questionario.id} onclick="responderQuestionario(${questionario.id})" class="cursor-pointer"
              data-bs-toggle="collapse" data-bs-target="#responder${questionario.id}"
              aria-expanded="false" aria-controls="responder${questionario.id}"> >
                <span>${questionario.titulo}</span>
            </div>
            <div id="responder${questionario.id}" class="collapse">
            </div>
            <hr />
        `)
    });
}

function responderQuestionario(questionarioId) {
    $("#responder"+questionarioId).html(`
        <div id='form-responder'>
            <div id='input-resposta' class='row'>
                <label for="inputResposta${questionarioId}" class="form-label">Resposta</label>
                <input type="text" class="form-control" name="Descricao" id="inputResposta${questionarioId}"></input>
            </div>
            <div class='row'>
                <div class='col'>
                    <label for="inputLatitude${questionarioId}" class="form-label">Latitude</label>
                    <input type="text" class="form-control" name="Latitude" id="inputLatitude${questionarioId}"></input>
                </div>
                <div class='col'>
                    <label for="inputLongitude${questionarioId}" class="form-label">Longitude</label>
                    <input type="text" class="form-control" name="Longitude" id="inputLongitude${questionarioId}"></input>
                </div>    
            </div>
            <button class="btn btn-primary" type="button" id="inserir-resposta" onclick="inserirResposta(${questionarioId})">
                Enviar Resposta
            </button>
        </div>
    `)
}

function inserirResposta(questionarioId) {
    $.ajax({
        type: "POST",
        url: getApi() + "Resposta",
        data: JSON.stringify(retornaDadosResposta(questionarioId)),
        cache: false,
        contentType: "application/json;charset=utf-8",
        success: function (retorno) {
            alert('Resposta registrada com sucesso!')
            responderIndex();
        },
        error: function () {
            alert("Ocorreu um erro ao salvar resposta");
        }
    });
}

function retornaDadosResposta(questionarioId) {
    var dados = {
        IdQuestionario: questionarioId.toString(),
        Usuario: $("#user").val(),
        Descricao: $("#inputResposta"+questionarioId).val(),
        Latitude: $("#inputLatitude"+questionarioId).val(),
        Longitude: $("#inputLongitude"+questionarioId).val()
    }
    return dados;
}
